---
clavier: false
theme: bubbles
maths: true
footer : false
typeWriter: true
addOns: kroki
---

# Modifié par Manue Aide pour les Triangles Rectangles

Bienvenue ! Je peux t'aider à trouver la valeur d'une longueur ou d'un angle inconnu dans un triangle rectangle. De quelles données disposes-tu ?

<!-- Cette ligne est masquée et ne sera pas affichée -->
![Triangle pour Pythagore](https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/images/HdeOBot_images/chat-bot3.png)
<!-- Cette ligne est masquée et ne sera pas affichée -->

1. [Deux longueurs et une longueur inconnue](Pythagore)
2. [Une longueur et un angle pour trouver une autre longueur](TrigonometrieLongueur)
3. [Deux longueurs pour trouver un angle](TrigonometrieAngle)

## Pythagore
!Audio:https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/sons/HdeO_sons/607207__fupicat__congrats.wav

Pour utiliser le théorème de Pythagore, indique quelles sont les longueurs que tu connais :

1. [Je connais les longueurs des deux côtés](PythagoreHypotenuse)
2. [Je connais la longueur d'un côté et de l'hypoténuse](PythagoreCote)

## TrigonometrieLongueur

Pour utiliser les formules de trigonométrie, indique quelle longueur et quel angle tu connais :

1. [Je connais la longueur du côté adjacent et un angle](LongueurAdjacente)
2. [Je connais la longueur du côté opposé et un angle](LongueurOpposee)
3. [Je connais la longueur de l'hypoténuse et un angle](LongueurHypotenuse)

## TrigonometrieAngle

Pour trouver un angle à partir de deux longueurs, indique quelles longueurs tu connais :

1. [Je connais les longueurs des deux côtés](AngleDeuxCotes)
2. [Je connais la longueur du côté opposé et de l'hypoténuse](AngleOpposeHypotenuse)
3. [Je connais la longueur du côté adjacent et de l'hypoténuse](AngleAdjacenteHypotenuse)

## PythagoreHypotenuse

Pour trouver l'hypoténuse, utilise le théorème de Pythagore :
$$ c = \sqrt{a^2 + b^2} $$
où $a$ et $b$ sont les longueurs des deux côtés.

<!-- Cette ligne est masquée et ne sera pas affichée -->

![Triangle pour Pythagore](https://i.ibb.co/qJxHS91/Triangle1.png)

**Exemple :**
Si $a = 3$ et $b = 4$, alors :
$$ c = \sqrt{3^2 + 4^2} = \sqrt{9 + 16} = \sqrt{25} = 5 $$

1. [Retour au menu principal]()

## PythagoreCote

Pour trouver un côté, utilise le théorème de Pythagore :
$$ a = \sqrt{c^2 - b^2} $$
où $c$ est la longueur de l'hypoténuse et $b$ est la longueur de l'autre côté.

![Triangle pour Pythagore](https://i.ibb.co/qJxHS91/Triangle1.png)

**Exemple :**
Si $c = 5$ et $b = 4$, alors :
$$ a = \sqrt{5^2 - 4^2} = \sqrt{25 - 16} = \sqrt{9} = 3 $$

1. [Retour au menu principal]()

## LongueurAdjacente

Pour trouver une longueur à l'aide de l'angle et du côté adjacent, utilise :
$$ \text{côté opposé} = \text{adjacent} \times \tan(\theta) $$
$$ \text{hypoténuse} = \dfrac{\text{adjacent}}{\cos(\theta)} $$
où $\theta$ est l'angle connu.

![Triangle pour Pythagore](https://i.ibb.co/BsgKDqL/Triangle2.png)

**Exemple :**
Si $\text{adjacent} = 5$ et $\theta = 30^\circ$, alors :
$$ \text{côté opposé} = 5 \times \tan(30^\circ) \approx 5 \times 0.577 \approx 2.89 $$
$$ \text{hypoténuse} = \dfrac{5}{\cos(30^\circ)} \approx \dfrac{5}{0.866} \approx 5.77 $$

1. [Retour au menu principal]()

## LongueurOpposee

Pour trouver une longueur à l'aide de l'angle et du côté opposé, utilise :
$$ \text{côté adjacent} = \dfrac{\text{opposé}}{\tan(\theta)} $$
$$ \text{hypoténuse} = \dfrac{\text{opposé}}{\sin(\theta)} $$
où $\theta$ est l'angle connu.

![Triangle pour Pythagore](https://i.ibb.co/BsgKDqL/Triangle2.png)

**Exemple :**
Si $\text{opposé} = 3$ et $\theta = 30^\circ$, alors :
$$ \text{côté adjacent} = \dfrac{3}{\tan(30^\circ)} \approx \dfrac{3}{0.577} \approx 5.2 $$
$$ \text{hypoténuse} = \dfrac{3}{\sin(30^\circ)} = \dfrac{3}{0.5} = 6 $$

1. [Retour au menu principal]()

## LongueurHypotenuse

Pour trouver une longueur à l'aide de l'angle et de l'hypoténuse, utilise :
$$ \text{côté adjacent} = \text{hypoténuse} \times \cos(\theta) $$
$$ \text{côté opposé} = \text{hypoténuse} \times \sin(\theta) $$
où $\theta$ est l'angle connu.

![Triangle pour Pythagore](https://i.ibb.co/BsgKDqL/Triangle2.png)

**Exemple :**
Si $\text{hypoténuse} = 10$ et $\theta = 45^\circ$, alors :
$$ \text{côté adjacent} = 10 \times \cos(45^\circ) \approx 10 \times 0.707 \approx 7.07 $$
$$ \text{côté opposé} = 10 \times \sin(45^\circ) \approx 10 \times 0.707 \approx 7.07 $$

1. [Retour au menu principal]()

## AngleDeuxCotes

Pour trouver un angle à l'aide des longueurs des deux côtés, utilise :
$$ \theta = \arctan\left(\dfrac{\text{opposé}}{\text{adjacent}}\right) $$

![Triangle pour Pythagore](https://i.ibb.co/BsgKDqL/Triangle2.png)

**Exemple :**
Si $\text{opposé} = 4$ et $\text{adjacent} = 3$, alors :
$$ \theta = \arctan\left(\dfrac{4}{3}\right) \approx 53.13^\circ $$

1. [Retour au menu principal]()

## AngleOpposeHypotenuse

Pour trouver un angle à l'aide du côté opposé et de l'hypoténuse, utilise :
$$ \theta = \arcsin\left(\dfrac{\text{opposé}}{\text{hypoténuse}}\right) $$

![Triangle pour Pythagore](https://i.ibb.co/BsgKDqL/Triangle2.png)

**Exemple :**
Si $\text{opposé} = 3$ et $\text{hypoténuse} = 5$, alors :
$$ \theta = \arcsin\left(\dfrac{3}{5}\right) \approx 36.87^\circ $$

1. [Retour au menu principal]()

## AngleAdjacenteHypotenuse

Pour trouver un angle à l'aide du côté adjacent et de l'hypoténuse, utilise :
$$ \theta = \arccos\left(\dfrac{\text{adjacent}}{\text{hypoténuse}}\right) $$

![Triangle pour Pythagore](https://i.ibb.co/BsgKDqL/Triangle2.png)

**Exemple :**
Si $\text{adjacent} = 4$ et $\text{hypoténuse} = 5$, alors :
$$ \theta = \arccos\left(\dfrac{4}{5}\right) \approx 36.87^\circ $$

1. [Retour au menu principal]()
