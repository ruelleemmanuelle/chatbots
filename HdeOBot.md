---
clavier: true
obfuscate: true
tags : chatbot,HdeOBot, education
gestionGrosMots: true
maths: true
avatar: https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/images/HdeOBot_images/chatbot.png?ref_type=heads
footer: CC BY NC SA Emmanuelle Ruelle 2024 - Créé avec ChatMD, un outil libre et gratuit de Cédric Eyssette et très inspiré du BoBot de Mélanie Fenaert
rechercheContenu: false
style: audio{visibility:hidden}
---

# HdeOBot

Bonjour, je suis HdeOBot !
Je suis le chatbot qui te guide pour réviser ce chapitre.
![](https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/blob/master/images/HdeOBot_images/chat-bot3.png)

Que souhaites-tu faire aujourd'hui ?

1. [Voir ce que tu dois savoir en fin de chapitre](Objectifs)
2. [Réviser avec les flashcards](Flashcards)
3. [Avoir une vue d'ensemble du cours en image](Carte_conceptuelle)

## Objectifs

Ce chapitre est **essentiel** : c'est la base de tout le thème de mécanique


1. [Veux-tu voir la liste des prérequis?](prerequis)
2. [Je veux uniquement voir ce que je dois savoir](cequejedoissavoir)




## prerequis
<br>
• Est-ce que je sais ce qu’est le vecteur position, le vecteur vitesse ?
•  Est-ce que je connais des exemples de forces ?
•  Est-ce que je connais le lien entre la somme des forces extérieures et la variation du vecteur vitesse ?
•  Est-ce que je connais le principe d’inertie ?
•  Est-ce que je sais tracer le vecteur variation de vitesse ?
<br>

Que veux-tu découvrir maintenant ?

1. [Je veux voir ce que je dois savoir](cequejedoissavoir)
2. [Réviser avec les flashcards](Flashcards)
3. [Avoir une vue d'ensemble du cours en image](Carte_conceptuelle)
4. [Retour au message initial]()
5. [Finir notre colaboration pour cette fois-ci](Fin)

## cequejedoissavoir

!Audio:https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/sons/HdeO_sons/270330__littlerobotsoundfactory__jingle_achievement_01.wav

>:::success
>Est-ce que je sais définir le vecteur vitesse comme la dérivée du vecteur position par rapport au temps ?
> Est-ce que je sais définir le vecteur accélération comme la dérivée du vecteur vitesse par rapport au temps ?
> Est-ce que je sais établir les coordonnées cartésiennes du vecteur vitesse et du vecteur accélération à partir des coordonnées du vecteur position et/ou du vecteur vitesse ?
> Est-ce que je sais citer et exploiter les expressions des coordonnées des vecteurs vitesse et accélération dans le repère de Frenet, dans le cas d’un mouvement circulaire ?
> Est-ce que je sais caractériser le vecteur accélération pour les mouvements suivants : rectiligne, rectiligne uniforme, rectiligne uniformément accéléré, circulaire, circulaire uniforme ?
> Est-ce que je sais que le centre de masse d’un système est le point du système pour lequel l’étude du mouvement est le plus simple ?
> Est-ce que je sais qu’est un référentiel galiléen ?
> Est-ce que je sais utiliser la deuxième loi de Newton pour en déduire le vecteur accélération ou la valeur d’une force ?
<br>
> _Activités pratiques_
>Est-ce que je sais exploiter une vidéo ou une chronophotographie pour déterminer les coordonnées du vecteur position en fonction du temps et en déduire les coordonnées approchées ou les représentations des vecteurs vitesse et accélération
<br>
> _Capacité numérique_
>Représenter, à l'aide d'un langage de programmation, des vecteurs accélération d'un point lors d'un mouvement.
::: 

Que veux-tu découvrir maintenant ?

1. [Réviser avec les flashcards](Flashcards)
2. [Avoir une vue d'ensemble du cours en image](Carte_conceptuelle)
3. [Retour au message initial]()
4. [Finir notre colaboration pour cette fois-ci](Fin)


## Flashcards

C'est un très bon choix, C'est parti!!!

!Audio:https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/sons/HdeO_sons/607207__fupicat__congrats.wav


<iframe src="https://ladigitale.dev/digiflashcards/#/f/65ce8be1361f6" allowfullscreen frameborder="0" width="100%" height="500"></iframe>

Que veux-tu faire maintenant ?

1. [Voir ce que tu dois savoir en fin de chapitre](Objectifs)
2. [Avoir une vue d'ensemble du cours en image](Carte_conceptuelle)
3. [Retour au message initial]()
4. [Finir notre colaboration pour cette fois-ci](Fin)

## Carte_conceptuelle
- image du cours
- résumé

Tu aimes avoir une vue d'ensemble tu as raison
!Audio:https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/sons/HdeO_sons/256543__debsound__r2d2-astro-droid.wav

![carte_mentale_chapitre] (https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/images/cartes_conceptuelles/CM_Physique2p1_TGSp%C3%A9.png)

Que veux-tu faire maintenant ?

1. [Voir ce que tu dois savoir en fin de chapitre](Objectifs)
2. [Réviser avec les flashcards](Flashcards)
3. [Retour au message initial]()
4. [Finir notre colaboration pour cette fois-ci](Fin)

## Fin
A bientôt pour une collaboration qui j'espère ta été profitable !
!Audio:https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/sons/HdeO_sons/256543__debsound__r2d2-astro-droid.wav

![Abientôt](https://forge.apps.education.fr/ruelleemmanuelle/chatbots/-/raw/master/images/HdeOBot_images/chat-bot3.png =200x180)


