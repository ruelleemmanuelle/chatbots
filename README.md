# Chatbots

## Description

Chatbots à vocation pédagogique, créés par Emmanuelle Ruelle avec l'outil ChatMD de Cédric Eyssette et l'aide de Mélanie Fenaert

- HdeObot : pour s'entraîner à créer un chatbot grâce au modèle du BoBot de Mélanie Fenaert
https://chatmd.forge.apps.education.fr/#https://mfenaert.forge.apps.education.fr/chatbots/BoBot.md

- RuellePCbot : un assistant de navigation mon site https://www.ruellepc.com/

## Licence

CC BY NC SA 



